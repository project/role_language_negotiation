# INTRODUCTION
  Role Language Negotiation module provides the ability for the site
  administrator to configure language negotiation depending on user roles.

# REQUIREMENTS
  This module require language module in Drupal core.

# INSTALLATION
  Open your terminal and download with composer.
  composer require drupal/role_language_negotiation

  OR

  1. Download the module to your DRUPAL_ROOT/modules directory,
     or where ever you install contrib modules on your site.
  2. Go to Admin > Extend and enable the module.

# CONFIGURATION
  1. Go to /admin/config/regional/language/detection
  2. Enable User Role language negotiation and choose it's weight.
  3. Go to /admin/config/regional/language/detection/role
  4. Configure the roles languages.
  6. Configure the method of determining languages from /admin/config/regional/language/detection/url
  5. Done.
  Note: No need to enable URL language detection.

# AUTHOR
  Anas Mawlawi
  anas.mawlawi89@gmail.com
