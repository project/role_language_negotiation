<?php

namespace Drupal\role_language_negotiation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;

/**
 * Class NegotiationRoleForm.
 */
class NegotiationRoleForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'role_language_negotiation_configure_roles_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['role_language_negotiation.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $negotiation = $this->config('role_language_negotiation.settings')
      ->get('language_negotiation_roles');

    $roles = user_role_names();
    $form['roles'] = [
      '#type' => 'table',
      '#caption' => $this->t('Preferred language per role. User with multiple roles will be evaluated by the order of roles configured here.'),
      '#header' => [
        $this->t('Role'),
        $this->t('Language'),
        $this->t('Weight'),
      ],
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'weight',
        ],
      ],

    ];

    // Remove any unknown role.
    $negotiation = array_intersect_key($negotiation, $roles);

    // Sort roles, roles from config first.
    $sortedRoles = array_merge($negotiation, $roles);

    $weight = 20;
    foreach ($sortedRoles as $role_id => $role_name) {
      $form['roles'][$role_id]['#attributes']['class'][] = 'draggable';

      $form['roles'][$role_id]['role_name']['#markup'] = $role_name;
      $form['roles'][$role_id]['preferred_language'] = [
        '#type' => 'language_select',
        '#languages' => LanguageInterface::STATE_CONFIGURABLE,
        '#default_value' => $negotiation[$role_id] ?? '',
        '#empty_option' => $this->t('- No preference -'),
        '#empty_value' => '',
      ];
      $form['roles'][$role_id]['weight'] = [
        '#type' => 'weight',
        '#title' => $this->t('Weight'),
        '#title_display' => 'invisible',
        '#default_value' => $weight,
        '#attributes' => ['class' => ['weight']],
      ];
      $weight--;
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValue('roles');

    $filtered = array_filter($values, function ($value) {
      return !empty($value['preferred_language']);
    });

    $result = array_map(function ($value) {
      return $value['preferred_language'];
    }, $filtered);

    $config = $this->config('role_language_negotiation.settings');
    $config->set('language_negotiation_roles', $result);
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
