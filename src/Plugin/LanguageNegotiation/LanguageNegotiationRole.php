<?php

namespace Drupal\role_language_negotiation\Plugin\LanguageNegotiation;

use Drupal\language\LanguageNegotiationMethodBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class for setting the language based on the user role.
 *
 * @LanguageNegotiation(
 *   id = Drupal\role_language_negotiation\Plugin\LanguageNegotiation\LanguageNegotiationRole::METHOD_ID,
 *   weight = 50,
 *   name = @Translation("User role"),
 *   description = @Translation("Language based on the user role."),
 *   config_route_name = "role_language_negotiation.language.negotiation_role"
 * )
 */
class LanguageNegotiationRole extends LanguageNegotiationMethodBase {

  /**
   * The language negotiation method id.
   */
  const METHOD_ID = 'language-role';

  /**
   * {@inheritdoc}
   */
  public function getLangcode(Request $request = NULL) {
    $preferred_langcode = $this->getPreferredLangcode();
    if (empty($preferred_langcode)) {
      // No preference.
      return NULL;
    }

    if (empty($this->languageManager)) {
      return NULL;
    }

    $languages = $this->languageManager->getLanguages();
    if (!isset($languages[$preferred_langcode])) {
      // Preferred language not available.
      return NULL;
    }

    return $preferred_langcode;
  }

  /**
   *  Language preference by role.
   *
   * @return string|null
   *   Preferred language code for current user based on user role and role
   *   language negotiation settings.
   */
  function getPreferredLangcode(): ?string {
    $user_roles = $this->currentUser->getRoles();

    $negotiation = $this->config->get('role_language_negotiation.settings')
      ->get('language_negotiation_roles');

    if (empty($negotiation)) {
      // Language negotiation by role not configured.
      return NULL;
    }

    $roles = array_intersect_key($negotiation, array_flip($user_roles));
    if (empty($roles)) {
      // No language negotiation by role applies for this user.
      return NULL;
    }

    // Return the first language code from the list.
    return reset($roles);
  }

}
